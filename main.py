# Module specific libraryfrom dth import DTH
from bmp085 import BMP180
from dth import DTH
import time
from machine import Pin, I2C
from pycom import rgbled, heartbeat
print("Inicia el Main")

heartbeat(False)
rgbled(0X001010)  # turquoise

# this example assumes the default connection for the I2C Obviously
# at P9 = sda and P10 = scl
i2c = I2C()

bmp = BMP180(i2c)
rgbled(0X220000)  #red colour

bmp.oversample = 2
bmp.sealevel = 101325

# Wire connection over P8 pin for a DHT11 sensor
th = DTH(Pin('P8', mode=Pin.OPEN_DRAIN), 0)
time.sleep(2)

dth_temp_tens=1
dth_temp_ones=2
dth_humidity_tens=3
dth_humnidity_ones=4
bmp_pressure_thousands=5
bmp_pressure_hundreds=6
bmp_pressure_tens=7
bmp_pressure_ones=8
bmp_temperature_tens=9
bmp_temperature_ones=10
bmp_temperature_tenths=11
bmp_temperature_hundredths=12

if(1):
    # DTH11 acquiring data
    dth_result = th.read()
    if dth_result.is_valid():
        rgbled(0x001000)  # red
        #print("DTH: Temperature: {} C".format(dth_result.temperature) +
        #    " - " + "Humidity: %d %%" % dth_result.humidity)
        # print("Humidity: %d %%" % result.humidity)
        print('DTH: Temperature: {}'.format(dth_result.temperature) + 
                ' - Humidity: {}'.format(dth_result.humidity)
                )
        
    else:
        rgbled(0x110000)  # green
        print("Resultado del DTH11 Invalido")
    time.sleep(2)

    # BMP180 acquiring data
    bmp_temperature = bmp.temperature
    bmp_pressure = bmp.pressure
    bmp_altitude = bmp.altitude
    print("BMP: Pressure: " + str(bmp_pressure) + " - " + "Temp: " +
        str(bmp_temperature) + " - " + "Altitude: " + str(bmp_altitude))

    
print("Termina el Main")

# DTH Temp and Humidity
dth_temp_tens=int(dth_result.temperature/10)
dth_temp_ones=int(dth_result.temperature%10)
dth_humidity_tens=int(dth_result.humidity/10)
dth_humnidity_ones=int(dth_result.humidity%10)

# BMP Pressure
pressure=bmp.pressure
bmp_pressure_ones=int(pressure%10)
bmp_pressure_tens=int(pressure/10%10)
bmp_pressure_hundreds=int(pressure/100%10)
bmp_pressure_thousands=int(pressure/1000%10)

# BMP Temp
temperature=bmp_temperature
bmp_temperature_hundredths=int(temperature*100%10)
bmp_temperature_tenths=int(temperature*10%10)
bmp_temperature_ones=int(temperature%10)
bmp_temperature_tens=int(temperature/10%10)

message=[
        dth_temp_tens, dth_temp_ones,  # 2bytes Temperatur
        dth_humidity_tens, dth_humnidity_ones, # 2bytes Humidity
        bmp_pressure_thousands, bmp_pressure_hundreds, bmp_pressure_tens, bmp_pressure_ones, # 4bytes pressure
        bmp_temperature_tens, bmp_temperature_ones, bmp_temperature_tenths, bmp_temperature_hundredths # 2byte temperature real 2bytes decimal
        ]
print(message)

print("INICIA COMUNICACION SIGFOX")
from network import Sigfox
import socket
import binascii

# init Sigfox for RCZ4 (America)
sigfox = Sigfox(mode=Sigfox.SIGFOX, rcz=Sigfox.RCZ4)

# create a Sigfox socket
s = socket.socket(socket.AF_SIGFOX, socket.SOCK_RAW)

print('I am device ',  binascii.hexlify(sigfox.id()) )

# make the socket blocking
s.setblocking(True)

# configure it as uplink only
s.setsockopt(socket.SOL_SIGFOX, socket.SO_RX, False)

s.send(bytes(message))
print("FINALIZA COMUNICACION SIGFOX")
